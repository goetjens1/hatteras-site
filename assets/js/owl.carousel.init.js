document.addEventListener("DOMContentLoaded", function(event) {
	$(document).ready(function(){
		$(".owl-carousel").owlCarousel({
			center: true,
			items: 2,
			loop: true,
			autoplay: true,
			autoplayTimeout: 4000,
			autoplayHoverPause: true,
			nav: true,
			navText: ["<img src='assets/images/arrow-left.png'>","<img src='assets/images/arrow-right.png'>"]
		});
	}); 
});