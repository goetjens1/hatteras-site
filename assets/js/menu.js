$(document).ready(function() {
    var touch = $('#touch-menu');
    var menu = $('.menu');
    $(touch).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
    });
    $(window).resize(function() {
        var w = $(window).width();
        if (w > 767 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
		// onclick for mobile
		if (w <= 768) {
			var menu_li = $('.menu li');
			//menu_li.css("cursor", "pointer");
			menu_li.click(function(event) {
				event.stopPropagation();
				var child = $(this).find('>ul');
				if (child.css("display") == "none") {
					child.css("display", "block");
				}
				else {
					child.css("display", "none");
				}
				
				
			});
		}
    });
	// call resize function after load.
	$(window).resize();
});